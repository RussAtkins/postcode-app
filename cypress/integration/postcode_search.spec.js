context('Search for postcode', () => {
    beforeEach(() => {
        cy.visit('http://localhost:8080')
    })

    // describe('Input autocomplete', () => {
    //     it('Can search for an address', () => {
    //         cy.log('Fill in autocomplete')
    //         cy.get('#autocomplete')
    //         .type('31 fairways drive ellesmere', { delay: 80 })
    //     })
    // })

    it('Should search for a postcode', () => {
        cy.get('#showPostcodeSearch')
        .click()

        cy.get('.backupSearch')
        .should('be.visible')

        cy.get('#postcode')
        .type('CH66 1RX')

        cy.get('#postcodeSearch')
        .click()

        cy.get('.addressList')
        .should('have.class', 'loaded')
    })

    it('Should be able to toggle postcode search', () => {
        cy.get('#showPostcodeSearch')
        .click()

        cy.get('.backupSearch')
        .should('be.visible')

        cy.get('#showPostcodeSearch')
        .click()

        cy.get('.backupSearch')
        .should('not.be.visible')
    })

});