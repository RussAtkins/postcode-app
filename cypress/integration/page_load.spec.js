context('Page load', () => {
    beforeEach(() => {
        cy.visit('http://localhost:8080')
    })

    describe('Page Basics', () => {
        it('Should have loaded a map', () => {
            cy.get('.gm-style')
            .should('be.visible')
        })

        it('Should have an input field for addresses', () => {
            cy.get('#autocomplete')
            .should('be.visible')
        })

        it('Should have a button to open postcode search', () => {
            cy.get('#showPostcodeSearch')
            .should('be.visible')
        })
    })

});