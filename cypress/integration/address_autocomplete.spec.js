context('Search for address', () => {
    beforeEach(() => {
        cy.visit('http://localhost:8080')
    })

    describe('Input autocomplete', () => {
        it('Can search for an address', () => {
            cy.log('Fill in autocomplete')
            cy.get('#autocomplete')
            .type('31 fairways drive ellesmere', { delay: 80 })

            cy.log('List of addresses should be visible')
            cy.get('.pac-container')
            .should('be.visible')

            cy.log('Select an address')
            cy.get('.pac-item')
            .click()

            cy.log('Address list should have hidden')
            cy.get('.pac-container')
            .should('not.be.visible')

            cy.log('Should have minimised the blurred overlay')
            cy.get('.foundaddress')
            .should('be.visible')

            cy.log('Should have added a pin to the map')
            cy.get('[usemap="#gmimap0"]')
            .should('be.visible')

            cy.log('Should have added a pin to the map')
            cy.get('.submitButton')
            .click()
            cy.get('#autocomplete')
            .should('be.visible')
        })

        it('Can search for a postcode', () => {
            cy.log('Fill in autocomplete')
            cy.get('#autocomplete')
            .type('CH66 1RX', { delay: 80 })

            cy.log('List of addresses should be visible')
            cy.get('.pac-container')
            .should('be.visible')

            cy.log('Select an address')
            cy.get('.pac-item')
            .click()

            cy.log('Address list should have hidden')
            cy.get('.pac-container')
            .should('not.be.visible')

            // cy.log('Should have minimised the blurred overlay')
            // cy.get('.foundaddress')
            // .should('be.visible')

            // cy.log('Should have added a pin to the map')
            // cy.get('[usemap="#gmimap0"]')
            // .should('be.visible')

            // cy.log('Should have added a pin to the map')
            // cy.get('.submitButton')
            // .click()
            // cy.get('#autocomplete')
            // .should('be.visible')
        })

        it('Enters bad data', () => {
            cy.log('Fill in autocomplete')
            cy.get('#autocomplete')
            .type('@@()', { delay: 80 })

            cy.log('List of addresses should NOT be visible')
            cy.get('.pac-container')
            .should('not.be.visible')
        })

        it('Searches for an area', () => {
            cy.log('Fill in autocomplete')
            cy.get('#autocomplete')
            .type('little sutton', { delay: 80 })

            cy.log('List of addresses should be visible')
            cy.get('.pac-container')
            .should('be.visible')

            cy.log('Select an address')
            cy.get('.pac-item:first')
            .click()

            cy.log('Address list should have hidden')
            cy.get('.pac-container')
            .should('not.be.visible')
        })
    })

});