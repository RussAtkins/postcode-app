import Vue from "vue";
import Vuex from "vuex";
 
Vue.use(Vuex);
 
export default new Vuex.Store({
  state: {
    location: {},
    addresses: {},
    postcodeServiceState: '',
    showLocation: false ,
    showBackupSearch: false,
    postcode: '',
    showAddressList: false
  },
  getters: {},
  mutations: {
    movemap (state, location) {
        state.location = location
    },
    serviceState (state, status) {
        state.postcodeServiceState = status
    },
    showLocation (state, toggle) {
        state.showLocation = toggle
    },
    showBackupSearch (state, toggle) {
      state.showBackupSearch = toggle
    },
    currentPostcode (state, postcode) {
      state.postcode = postcode.toLowerCase().replace(/ /g,"")
    },
    showAddressList (state, toggle) {
      state.showAddressList = toggle
    },
    storeAddresses (state, data) {
      state.addresses = data
    },
  },
  actions: {
    movemap (context, location) {
      context.commit('movemap', location)
    },
    serviceState (context, status) {
      context.commit('serviceState', status)
    },
    showLocation (context, toggle) {
      context.commit('showLocation', toggle)
    },
    showBackupSearch (context, toggle) {
      context.commit('showBackupSearch', toggle)
    },
    currentPostcode (context, postcode) {
      context.commit('currentPostcode', postcode)
    },
    showAddressList (context, toggle) {
      context.commit('showAddressList', toggle)
    },
    storeAddresses (context, data) {
      context.commit('storeAddresses', data)
    },
  }
});